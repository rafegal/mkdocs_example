function removeElement(class_name) {
    var copyright = document.getElementsByClassName(class_name)[0];
    console.log(copyright);
    copyright.parentNode.removeChild(copyright);
}

function addElement(parentClass, html) {
    var p = document.getElementsByClassName(parentClass)[0];
    var node = document.createElement("div"); 
    node.innerHTML = html;
    p.appendChild(node);
}

html_copyright = '<div class="md-footer-copyright"><div class="md-footer-copyright__highlight">Copyright © 2022 - 2022 Rafael Galleani</div></div>';
removeElement('md-footer-copyright');
addElement('md-footer-meta__inner md-grid', html_copyright);
